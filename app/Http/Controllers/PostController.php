<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Models\Image;
use App\Models\Phone;
use App\Models\Comment;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = User::all();
        // $user = User::find(5)->phone;
        // $phone = Phone::find(10)->user;
        // $posts = User::find(5);

        // $post = Post::find(2);
        // $user = User::find(10);
        // $post = Post::whereBelongsTo($user)->get();

        // $latestPost = User::find(10)->latestPost;
        // $oldestPost = User::find(10)->userComment;

        // $data = Post::with(['comments' => function ($query) {
        //     $query->where('comment', 'like', '%'.'Velit et aliquid aperiam'.'%');
        // }])->get();

        // $data = Post::find(1);
        // $comment = new Comment(['comment' => 'A new comment.']);
        // $data->comments()->save($comment);

        // $data = User::find(5);
        // $post = new Post([
        //     'title' => 'This is a test post title'
        // ]);
        // $data->posts()->save($post);
        // $data->refresh();
        // All comments, including the newly saved comment...

        // $post = Post::find(25);
        // $comment = $post->comments()->create([
        //     'comment' => 'A new one comment.',
        // ]);
        // $comment = $post->comments()->createmany([
        //     ['comment' => 'A new 2 comment.'],
        //     ['comment' => 'Another new comment.'],
        // ]);

        // $post->comments[3]->comment = 'BEst Comment';

        // $post->push();


        // $phone = Phone::find(10);
        // $user = User::find(2);
        // $phone->user()->associate($user);
        // $phone->user()->dissociate($user);
        // $phone->save();

        $user = User::find(2);
        $post = Post::find(1);

        // Atach a single role from the user...
        // $post->user()->attach($user);

        //morphto method
        // $img = $post->image;
        // $img = $user->image;

        // $image = Image::find(2);
        // $imageable = $image->imageable;

        return $post->oldestImage;

        // return view('test', compact('posts'));
    }
}
