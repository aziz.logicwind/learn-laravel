<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    // protected $with = ['user','comments'];
    protected $fillable = ['title'];


    /**
     * Get the user that owns the post.
     */
    public function user()
    {
        // return $this->belongsTo(User::class);
        // or
        return $this->belongsTo(User::class, 'user_id', 'id'); //return $this->belongsTo(User::class, 'foreign_key', 'owner_key');
    }

    /**
     * Get the user that owns the post.
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get the post's image.
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable'); //return one image
        // return $this->morphMany(Image::class, 'imageable'); // return all related image
    }

    /**
     * Get the user's most recent image.
     */
    public function latestImage()
    {
        return $this->morphOne(Image::class, 'imageable')->latestOfMany();
    }

    /**
     * Get the user's oldest image.
     */
    public function oldestImage()
    {
        return $this->morphOne(Image::class, 'imageable')->oldestOfMany();
    }


    /**
     * Get the user's most popular image.
     */
    public function bestImage()
    {
        return $this->morphOne(Image::class, 'imageable')->ofMany('likes', 'max');
    }
}
