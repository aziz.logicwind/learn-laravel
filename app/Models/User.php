<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //one to one relation
    // pass a second argument as foregin key: & pass a third argument as local key:
    public function phone()
    {
        return $this->hasOne(Phone::class);
        // return $this->hasOne(Phone::class, 'user_id', 'id');
    }

    //one to many relation
    public function phones()
    {
        // return $this->hasMany(Phone::class)->latestOfMany();
        return $this->hasMany(Phone::class);
    }

    //one to many relation
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * Get the user's most recent Post.
     */
    public function latestPost()
    {
        // return $this->hasOne(Post::class)->ofMany('id', 'max');
        return $this->hasOne(Post::class)->latestOfMany();
    }

    /**
     * Get the user's oldest Post.
     */
    public function oldestPost()
    {
        return $this->hasOne(Post::class)->oldestOfMany();
    }

    /**
     * Get the comments's owner.
     */
    public function userComment()
    {
        return $this->hasOneThrough(
            Comment::class,
            Post::class,
            'user_id', // Foreign key on the posts table...
            'post_id', // Foreign key on the comments table...
            'id', // Local key on the users table...
            'id' // Local key on the posts table...
        );
    }

    /**
     * Get the user's image.
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
